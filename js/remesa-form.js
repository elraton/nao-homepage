document.addEventListener('DOMContentLoaded', function() {
  const exchangeButton = document.querySelector('.exchange-button');
  const currencyLabel1 = document.querySelector('.currency1-text');
  const currencyLabel2 = document.querySelector('.currency2-text');
  const currencyAmount1 = document.querySelector('.currency-amount1');
  const currencyAmount2 = document.querySelector('.currency-amount2');
  const menuButton = document.querySelector('.navbar-toggler');
  const menuLinks = document.querySelectorAll('.navbar-nav .nav-link');
  exchangeButton.onclick = function(e) {
    e.preventDefault();
    const textAux = currencyLabel1.innerHTML;
    const amountAux = currencyAmount1.value;
    currencyLabel1.innerHTML = currencyLabel2.innerHTML;
    currencyLabel2.innerHTML = textAux;

    currencyAmount1.value = currencyAmount2.value;
    currencyAmount2.value = amountAux;
  };

  for (const link of menuLinks) {
    link.onclick = function (e) {
      menuButton.click();
    }
  }
});